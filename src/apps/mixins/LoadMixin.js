export default {
    data() {
        return {
            loading: false, loaded: true
        }
    }, methods: {
        load() {
            this.beforeLoad()
            this.afterLoad()
        }, beforeLoad() {
            this.loaded = false
            this.loading = true
        }, afterLoad() {
            this.loaded = true
            this.loading = false
        }
    },
}
