export default {
  CustomFooter: () => import(/* webpackChunkName 'custom-footer' */ 'apps/components/CustomFooter'),
  CustomMain: () => import(/* webpackChunkName 'custom-main' */ 'apps/components/CustomMain'),
}
