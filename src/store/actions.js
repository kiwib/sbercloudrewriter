export default {
  addError({commit, state}, error) {
    commit('ADD_ERROR', error.error)
  }
}
