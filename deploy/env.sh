#!/bin/sh
set -e

cat /dev/null >.env
echo "PORT=$PORT" >>.env
echo "CONTAINER_IMAGE=$CONTAINER_IMAGE" >>.env
