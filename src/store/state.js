export default {
  AUTHOR_DATA: {
    name: 'Шайкин А.В.',
    contacts: [
      {icon: 'fab fa-github', url: 'https://github.com/kiwib0n', color: 'black'},
      {icon: 'fab fa-gitlab', url: 'https://gitlab.com/kiwib', color: 'amber darken-4'},
      {icon: 'fab fa-vk', url: 'https://vk.com/newdef', color: 'primary'}
    ],
    avatar: 'https://sun9-52.userapi.com/c848736/v848736741/6670a/6W6k1uGZHEs.jpg'
  },
  LOGGED_ERROR: []
}
