import Vue from 'vue'
import App from './App'
import vuetify from './plugins/vuetify'
import store from './store'

/* PLUGIN */
import ajax from './plugins/ajax'
import moment from './plugins/moment'
import logError from './plugins/logError'
import lodash from './plugins/lodash'
import apiPath from './plugins/apiPath'

/* STYLE */
import './style'

Vue.use(ajax)
Vue.use(apiPath)
Vue.use(moment)
Vue.use(lodash)
Vue.use(logError, {store})
/* ##### */
/* ##### */

new Vue({
    el: '#app', vuetify, store, render: h => h(App)
})
