export default {
    install(Vue, {store}) {
        Vue.prototype.$logError = error => {
            console.error(error)
            store.dispatch('addError', {error})
        }
    }
}
