# BUILD FRONTEND
FROM node:lts-alpine as build-stage

MAINTAINER Andrey Shaikin <kiwibon@yandex.ru>

ENV PROJECT_DIR=/opt/project
WORKDIR $PROJECT_DIR

RUN apk add --no-cache nodejs yarn python3 make g++

COPY package.*json webpack.config.js yarn.lock ./
COPY src ./src
COPY static ./static/

RUN true \
    && mkdir -p static \
     && yarn install --no-progress --non-interactive \
     && yarn cache clean \
     && yarn build

# production stage
FROM nginx:stable-alpine as production-stage
# очищаем от стандартных данных
RUN rm -rf /usr/share/nginx/html/
RUN rm -rf /etc/nginx/conf.d

COPY --from=build-stage /opt/project/dist /usr/share/nginx/html
COPY --from=build-stage /opt/project/static /usr/share/nginx/html/static

COPY index.html /usr/share/nginx/html/
COPY favicon.ico /usr/share/nginx/html/
RUN ls -la /usr/share/nginx/html/

COPY deploy/conf/conf.d/* /etc/nginx/conf.d/

EXPOSE 80
VOLUME ["/var/log", "/usr/share/nginx/html/static"]
CMD ["nginx", "-g", "daemon off;"]
