export default {
    install(Vue, options) {
        const proxy = 'https://cors-anywhere.shaykin-av.ru/'
        const url = 'https://api.aicloud.sbercloud.ru/public/v2/rewriter/predict'
        Vue.prototype.$apiPath = `${proxy}${url}`
    }
}
